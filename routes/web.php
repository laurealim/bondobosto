<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
//    return view('welcome');
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware' => ['role:admin|user|super-admin|upazilla-admin']], function () {

    //*  Country Route Section */
    Route::get('country/list', 'CountryController@list')->name('country.list');   // index
    Route::get('country/add', 'CountryController@form')->name('country.form');    // create
    Route::post('country/store', 'CountryController@store')->name('country.store');   // store
    Route::get('country/{id}', 'CountryController@edit')->name('country.edit');   // edit
    Route::post('country/{id}', 'CountryController@update')->name('country.update');    // update
    Route::delete('country/{id}', 'CountryController@destroy')->name('country.delete');    // update

    //*  Division Route Section */
    Route::get('division/list', 'DivisionController@list')->name('division.list');   // index
    Route::get('division/add', 'DivisionController@form')->name('division.form');    // create
    Route::post('division/store', 'DivisionController@store')->name('division.store');   // store
    Route::post('division/divisionSelectAjaxList', 'DivisionController@divisionSelectAjaxList')->name('division.divisionSelectAjaxList');    // Ajax Division List
    Route::get('division/{id}', 'DivisionController@edit')->name('division.edit');   // edit
    Route::post('division/{id}', 'DivisionController@update')->name('division.update');    // update
    Route::delete('division/{id}', 'DivisionController@destroy')->name('division.delete');    // delete

    //*  District Route Section */
    Route::get('district/list', 'DistrictController@list')->name('district.list');   // index
    Route::get('district/add', 'DistrictController@form')->name('district.form');    // create
    Route::post('district/store', 'DistrictController@store')->name('district.store');   // store
    Route::post('district/districtSelectAjaxList', 'DistrictController@districtSelectAjaxList')->name('district.districtSelectAjaxList');    // Ajax District List
    Route::get('district/{id}', 'DistrictController@edit')->name('district.edit');   // edit
    Route::post('district/{id}', 'DistrictController@update')->name('district.update');    // update
    Route::delete('district/{id}', 'DistrictController@destroy')->name('district.delete');    // delete

    //*  Upazila-- Route Section */
    Route::get('upazila/list', 'UpazilaController@list')->name('upazila.list');   // index
    Route::get('upazila/add', 'UpazilaController@form')->name('upazila.form');    // create
    Route::post('upazila/store', 'UpazilaController@store')->name('upazila.store');   // store
    Route::post('upazila/upazilaSelectAjaxList', 'UpazilaController@upazilaSelectAjaxList')->name('upazila.upazilaSelectAjaxList');    // Ajax Upazila List
    Route::get('upazila/{id}', 'UpazilaController@edit')->name('upazila.edit');   // edit
    Route::post('upazila/{id}', 'UpazilaController@update')->name('upazila.update');    // update
    Route::delete('upazila/{id}', 'UpazilaController@destroy')->name('upazila.delete');    // delete

    //*  Union Route Section */
    Route::get('union/list', 'UnionController@list')->name('union.list');   // index
    Route::get('union/add', 'UnionController@form')->name('union.form');    // create
    Route::post('union/store', 'UnionController@Store')->name('union.store');   // store
    Route::post('union/unionSelectAjaxList', 'UnionController@unionSelectAjaxList')->name('union.unionSelectAjaxList');    // Ajax District List
    Route::get('union/{id}', 'UnionController@edit')->name('union.edit');   // edit
    Route::post('union/{id}', 'UnionController@update')->name('union.update');    // update
    Route::delete('union/{id}', 'UnionController@destroy')->name('union.delete');    // delete

    Route::get('/home', 'HomeController@index')->name('dashboard');

//    Route::get('/roles', 'PermissionController@Permission');

//    Route::get('/admin', function () {
//        return 'Welcome Admin';
//    });

//    bondobosto route
//    Route::get('/applicant', 'ApplicanrController@index')->name('applicant.list');

//    Land Route
    Route::post('/lands/getLandInfoById', 'LandController@getLandInfoById')->name('lands.getLandInfoById');
    Route::resource('/lands', 'LandController');

//    Applicant Route
    Route::resource('/applicants', 'ApplicantController');

//    Land Assign Route
    Route::resource('/assign_lands', 'AssignLandController');

//    Role Routs
    Route::Resource('/roles', 'RoleController');
    Route::get('/role-setting', 'RoleController@roleSetting')->name('roles.roleSetting');
    Route::post('/role-setting','RoleController@roleSetting')->name('roles.roleSetting');
    Route::post('/update-role-setting','RoleController@updateRoleSetting')->name('roles.updateRoleSetting');

//    Permission Routs
    Route::resource('/permissions','PermissionController');

//    User Route
//    Route::get("/users/datatable", 'UserController@datatable')->name("users.datatable"); // for Data Table Export
//    Route::get("/users/datatable/getData", 'UserController@getData')->name("users.getData"); // for datatable Export
    Route::get("/users/export", 'UserController@export')->name("users.export"); // for datatable Export
    Route::Resource('/users', 'UserController');
    Route::get("/change-password", 'UserController@changepass')->name("users.changepass"); // for pass change
    Route::post("/change-password", 'UserController@changepass')->name("users.changepass"); // for pass change
    Route::post("/reset-password", 'UserController@resetpass')->name("users.resetpass"); // for pass reset

//    Cache Clear Route
    Route::get('/clear-cache', function() {
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        Artisan::call('config:cache');
        return "Cache cleared";
    });
});


//Route::group(['middleware' => ['role:developer|manager|superadmin']], function () {
//});

/*
Verb            URI                         Action          Route Name
GET             /photos                     index           photos.index
GET             /photos/create              create          photos.create
POST            /photos                     store           photos.store
GET             /photos/{photo}             show            photos.show
GET             /photos/{photo}/edit        edit            photos.edit
PUT/PATCH       /photos/{photo}             update          photos.update
DELETE          /photos/{photo}             destroy         photos.destroy
*/

