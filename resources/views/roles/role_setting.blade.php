@extends('layouts.app')


@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{!! $page_title !!}</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
{{--                        <form class="form-horizontal" role="form" action="{{ route('roles.store') }}" method="POST"--}}
{{--                              enctype="multipart/form-data" id="reg_form">--}}
{{--                            {{ csrf_field() }}--}}
                        <form method="POST" action="{{ route('roles.roleSetting') }}" id="quickForm">
                            @csrf
                            <div class="card-body">
                                <table id="role_lists" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        @if(count($roleListArr) > 0)
                                            @foreach($roleListArr as $role)
                                                <th>{!! $role->name !!}</th>
                                            @endforeach
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($permissionListArr) > 0)
                                        @foreach($permissionListArr as $s_page)
{{--                                            <tr>--}}
{{--                                                @foreach($roleListArr as $role)--}}
{{--                                                    <td><input--}}
{{--                                                            @if(isset($role_arr[$role->slug][$s_page->name]) && $role_arr[$role->slug][$s_page->name]=='on') checked--}}
{{--                                                            @endif type="checkbox"--}}
{{--                                                            name=role_arr[{!! $role->slug !!}][{!! $s_page->name !!}]> {!! $s_page->name !!}--}}
{{--                                                    </td>--}}
{{--                                                @endforeach--}}
{{--                                            </tr>--}}
                                            <tr>
                                                <?php
//                                                dd($roleListArr);
                                                ?>
                                                @foreach($roleListArr as $role)

                                                    <td>
{{--                                                        <input--}}
{{--                                                            @if(isset($role_arr[$role->id][$s_page->id]) && $role_arr[$role->id][$s_page->id]=='on') checked--}}
{{--                                                            @endif type="checkbox" id="{!! $role->id !!}_{!! $s_page->id !!}" class="roll_permission"--}}
{{--                                                            name=role_arr[{!! $role->id !!}][{!! $s_page->id !!}]> {!! $s_page->name !!}--}}
                                                        <input
                                                            @if(isset($rolPermArray[$role->id][$s_page->id]) && $rolPermArray[$role->id][$s_page->id]== $s_page->id) checked
                                                            @endif type="checkbox" id="{!! $role->id !!}_{!! $s_page->id !!}" class="roll_permission"
                                                            name=role_arr[{!! $role->id !!}][{!! $s_page->id !!}]> {!! $s_page->name !!}
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>

                                    <tfoot>
                                    </tfoot>
                                </table>
                                <br/>
                                <input type="submit" class="btn btn-primary float_right" value="Submit">
                            </div>

                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->




@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
        });

        $('.roll_permission').change(function (){
            // alert($(this).attr('id'));
            let flag = false;
            if ($(this).prop('checked')) {
                flag = true;
                alert('Checked');
            }else{
                flag = false;
                alert('Not Checked');
            }
            let url = "{{ route('roles.updateRoleSetting') }}";
            let ids = $(this).attr('id');
            let status = flag;
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {id: ids, status: status, "_token": "{{ csrf_token() }}"},

                success: function (data) {
                    if (data.status == 'success') {
                        window.location.reload();
                    } else if (data.status == 'error') {
                        console.log(data.msg);
                    }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                },
                error: function (data) {
                }
            });
        });
    </script>

@endsection
