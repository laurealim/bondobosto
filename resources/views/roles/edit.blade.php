@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('roles.update', $id) }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}


        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">ইউজার রোল</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-body -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ইউজার রোল নাম<span class="reqrd"> *</span></label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="কইউজার রোল নাম"
                                   required value="{{ $roleData->name }}">
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('name'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    {{--                    <div class="form-group">--}}
                    {{--                        <label>খতিয়ান</label>--}}
                    {{--                        <input type="text" class="form-control" placeholder="Enter ...">--}}
                    {{--                    </div>--}}
                    <!-- /.form-group -->
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>স্লাগ<span class="reqrd"></span></label>
                            <input type="text" id="name" name="slug" class="form-control" placeholder="স্লাগ"
                                   value="{{ $roleData->slug }}" readonly>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('slug'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    {{--                    <div class="form-group">--}}
                    {{--                        <label>খতিয়ান</label>--}}
                    {{--                        <input type="text" class="form-control" placeholder="Enter ...">--}}
                    {{--                    </div>--}}
                    <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.card-footer -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
        });

    </script>
@endsection
