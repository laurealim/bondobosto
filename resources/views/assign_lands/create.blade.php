@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" role="form" action="{{ route('assign_lands.store') }}" method="POST"
          enctype="multipart/form-data" id="reg_form">
        {{ csrf_field() }}

        <input type="hidden" id="case_no" name="case_no" value="">

        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">জমি বাছাই</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>জমি বাছাই <span class="reqrd"> *</span></label>
                            <select class="form-control select2bs4" id="land_id" name="land_id" required>
                                <option value="">--- বাছাই করুণ ---</option>
                                <?php
                                foreach ($landList as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('land_id'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('land_id') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group land_info">
                            <table id="land_info" class="table table-bordered data-table">
                                <thead>
                                <th>কেস নং</th>
                                <th>জেলা</th>
                                <th>উপজেলা জেলা</th>
                                <th>মৌজা</th>
                                <th>খতিয়ান</th>
                                <th>দাগ নং</th>
                                <th>জমির পরিমাণ</th>
                                <th>জমির মূল্য</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td id="td_case"></td>
                                    <td id="td_dist"></td>
                                    <td id="td_upa"></td>
                                    <td id="td_mouja"></td>
                                    <td id="td_khotiyan"></td>
                                    <td id="td_dag_no"></td>
                                    <td id="td_size"></td>
                                    <td id="td_price"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>

        <div class="card  card-primary">
            <div class="card-header">
                <h3 class="card-title">বন্দবস্থ বাছাই</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    {{--                <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
                    {{--                    <i class="fas fa-times"></i>--}}
                    {{--                </button>--}}
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>গ্রহণকারী বাছাই করুণ</label>
                            <select class="select2bs4" multiple="multiple" data-placeholder="Select a State"
                                    style="width: 100%;" name="clients[]" id="clients">
                                <?php
                                foreach ($cilentList as $id => $name){ ?>
                                <option value="{{ $id }}">{{ $name }}</option>
                                <?php }
                                ?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                            @if ($errors->has('clients'))
                                    <span class="help-block middle">
                                    <strong>{{ $errors->first('clients') }}</strong>
                                </span>
                                @endif
                        </span>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
            //Money Euro
            $('[data-mask]').inputmask()
        });

        $('#dist').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.upazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="upa"]').empty();
                upazillaList(districtID, token, url);
            } else {
                $('select[name="upa"]').empty();
            }
        });

        $('#upa').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.unionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="union"]').empty();
                unionList(upazilaID, token, url);
            }
        });

        $('#lnd_cat').on('change', function () {
            var sizeVal = $('#lnd_size').val();
            var catVal = $(this).val();
            if (sizeVal != null && sizeVal != '') {
                var calculation = (parseInt(catVal) * parseInt(sizeVal));
                $('#lnd_price').val(calculation);
            }

        });

        $('#lnd_size').on('change', function () {
            var catVal = $('#lnd_cat').val();
            var sizeVal = $(this).val();
            if (catVal != null && catVal != '') {
                var calculation = (parseInt(catVal) * parseInt(sizeVal));
                $('#lnd_price').val(calculation);
            }
        });

        $('#land_id').on('change', function () {
            var landId = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('lands.getLandInfoById') }}";
            {{--var value = "<?php getLandById(landId); ?>";--}}
            $.ajax({
                url: url,
                method: 'POST',
                data: {landId: landId, _token: token},
                dataType: "json",
                success: function (data) {
                    // console.log(data);
                    // $('select[name="upa"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (keys, values) {
                        // console.log(values.dag_no);
                        $("#td_case").html(values.case_no);
                        $("#td_dist").html(values.dist);
                        $("#td_upa").html(values.upa);
                        $("#td_mouja").html(values.mouja);
                        $("#td_khotiyan").html(values.khotiyan);
                        $("#td_dag_no").html(values.dag_no);
                        $("#td_size").html(values.lnd_size);
                        $("#td_price").html(values.lnd_price);
                        $('#case_no').val(values.case_no)
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        });

        function upazillaList(districtID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="upa"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $.each(data, function (key, value) {
                        $('select[name="upa"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="union"]').append('<option value="">' + "--- বাছাই করুন ---" + '</option>');
                    $('select[name="union"]').append('<option value="' + 0 + '">' + "পৌরসভা" + '</option>');

                    $.each(data, function (key, value) {
                        $('select[name="union"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

    </script>
@endsection
