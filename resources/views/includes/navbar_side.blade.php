<!-- Brand Logo -->
<a href="#" class="brand-link">
    {{--    <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">--}}
    <span class="brand-text font-weight-light">জমির বন্দবস্ত</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
{{--    <div class="user-panel mt-3 pb-3 mb-3 d-flex">--}}
{{--        <div class="image">--}}
{{--            <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">--}}
{{--        </div>--}}
{{--        <div class="info">--}}
{{--            <a href="#" class="d-block">Alexander Pierce</a>--}}
{{--        </div>--}}
{{--    </div>--}}

<!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu"
            data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="{{ route('dashboard') }}" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>ড্যাসবোর্ড
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('lands.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>জমির তথ্য
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('applicants.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>বন্দবস্ত গ্রহণকারীর তথ্য
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        জমির বন্দোবস্ত
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('assign_lands.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>বন্দোবস্তের ব্যবস্তা</p>
                        </a>
                    </li>
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="far fa-circle nav-icon"></i>--}}
                    {{--                            <p>Inactive Page</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                </ul>
            </li>
            <!--            --><?php
            //            if (check_view_role('super-admin')){
            //            ?>
            @role(array('super-admin'))
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        অনুমতি ব্যবস্তাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
{{--                    @can('create-task')--}}
                        <li class="nav-item">
                            <a href="{{ route('roles.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>ইউজার রোল</p>
                            </a>
                        </li>
{{--                    @endcan--}}
                    <li class="nav-item">
                        <a href="{{ route('permissions.index') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>ইউজার পারমিশন</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('roles.roleSetting') }}" class="nav-link active">
                            <i class="far fa-circle nav-icon"></i>
                            <p>রোল সেটিং</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        ইউজার ব্যবস্তাপনা
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
{{--                    @can('create-task')--}}
                        <li class="nav-item">
                            <a href="{{ route('users.index') }}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>সকল ইউজার</p>
                            </a>
                        </li>
{{--                    @endcan--}}
                </ul>
            </li>
            @endrole
            <!--            --><?php //} ?>
            {{--            <li class="nav-item">--}}
            {{--                <a href="#" class="nav-link">--}}
            {{--                    <i class="nav-icon fas fa-th"></i>--}}
            {{--                    <p>--}}
            {{--                        Simple Link--}}
            {{--                        <span class="right badge badge-danger">New</span>--}}
            {{--                    </p>--}}
            {{--                </a>--}}
            {{--            </li>--}}
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
