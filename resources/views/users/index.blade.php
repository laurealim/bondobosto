@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <div class="pad_space">
                            <a href="{{ route('users.create') }}" class="btn btn-block btn-secondary btn-md" title="Add">
                                <i class="far fa-plus-square fa-lg"></i>
                            </a>
                        </div>
                    </h3>
                    <h3 class="card-title">
                        <div class="pad_space">
                            <a href="{{ route('users.export') }}" class="btn btn-block btn-success btn-md" id="export_list" title="Download List">
                                <i class="fas fa-file-download fa-lg"></i>
                            </a>
                        </div>
                    </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered data-table">
                        {{--                    <table id="example1" class="table table-bordered table-striped">--}}
                        <thead>
                        <tr>
                            <th>ক্রমিক নং</th>
                            <th>নাম</th>
                            <th>ইমেইল</th>
                            <th>ইউজার রোল</th>
                            <th>জেলা</th>
                            <th>উপজেলা</th>
                            <th>এন,আই,ডি</th>
                            <th>লিঙ্গ</th>
                            <th>স্ট্যাটাস</th>
                            <th>ব্যবস্থাপনা</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_script')
    <script type="text/javascript">
        // $(function () {
        //     $(".data-table").DataTable({
        //         "responsive": true, "lengthChange": false, "autoWidth": false,
        //         "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        // });
        $(document).ready(function () {
            // $(".data-table").DataTable({
            //     "responsive": true, "lengthChange": false, "autoWidth": false,
            //     "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                lengthMenu: [[25, 50, 100, 150, -1], [25, 50, 100, 150, "All"]],
                pageLength: 25,
                buttons: [
                    {
                        extend: 'excel',
                        text: '<span class="fa fa-file-excel-o"></span> Excel Export',
                        exportOptions: {
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        }
                    }
                ],
                ajax: "{{ route('users.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'role_name', name: 'role_name'},
                    {data: 'dist_name', name: 'dist_name'},
                    {data: 'upaz_name', name: 'upaz_name'},
                    {data: 'nid', name: 'nid'},
                    {data: 'gender', name: 'gender'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $(document).on("click", "a.delete-user", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("Do you wat to delete?")) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(document).on("click", "a.pass-reset", function (ev) {
                ev.preventDefault();
                let url = $(this).attr("href");
                let id = $(this).attr("id");
                if (confirm("পাসওয়ার্ড রিসেট করতে চান?")) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {id: id, "_token": "{{ csrf_token() }}"},

                        success: function (data) {
                            if (data.status == 'success') {
                                window.location.reload();
                            } else if (data.status == 'error') {
                            }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                        },
                        error: function (data) {
                        }
                    });

                } else {
                    return false;
                }
            });

            $(document).on('click', '#export_lists', function (event){
                event.preventDefault();
                url = "{{ route('users.export') }}";

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {"_token": "{{ csrf_token() }}"},

                    success: function (data) {
                        if (data.status == 'success') {
                            window.location.reload();
                        } else if (data.status == 'error') {
                        }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                    },
                    error: function (data) {
                    }
                });
            });

        });
    </script>
@endsection


{{--@extends('layout/master')--}}
{{--@section('content')--}}
{{--    <!-- Main content -->--}}
{{--    <section class="content">--}}
{{--      <div class="container-fluid">--}}
{{--        <div class="row">--}}
{{--          <div class="col-12">--}}
{{--            <div class="card card-primary">--}}
{{--              <div class="card-header">--}}
{{--                <h3 class="card-title">{!! $title !!}</h3>--}}
{{--              </div>--}}
{{--              <!-- /.card-header -->--}}
{{--              <div class="card-body">--}}
{{--                <table id="role_lists" class="table table-bordered table-striped">--}}
{{--                  <thead>--}}
{{--                  <tr>--}}
{{--                    <th>নাম</th>--}}
{{--                    <th> স্লাগ </th>--}}
{{--                    <th style="width:17%!important;">Action</th>--}}
{{--                  </tr>--}}
{{--                  </thead>--}}
{{--                  <tbody>--}}
{{--                    @if(count($role_list) > 0)--}}
{{--                       @foreach($role_list as $role)--}}
{{--                  <tr onmouseover="ChangeBackgroundColor(this)" onmouseout="RestoreBackgroundColor(this)">--}}
{{--                    <td>{!! $role->role_name !!}</td>--}}
{{--                    <td>{!! $role->role_slug !!}</td>--}}



{{--                    <td style="width:17%!important;">--}}
{{--                      <a href="{{route('role.edit',$role->id)}}"><button class="btn-success">Edit</button> &nbsp;</a>--}}

{{--                      <a onclick="delete_data('role','{!! $role->id !!}','{!! $role->role_name !!}');" type="submit" name="submit"  data-toggle="modal" data-target="#modal-default"><button class="btn-danger">Delete</button></a>--}}


{{--                    </td>--}}


{{--                  </tr>--}}
{{--                </tbody>--}}
{{--                    @endforeach--}}
{{--                   @endif--}}
{{--                  </tfoot>--}}
{{--                </table>--}}
{{--              </div>--}}
{{--              @include('layout.delete-data-modal')--}}
{{--              <!-- /.card-body -->--}}
{{--            </div>--}}
{{--            <!-- /.card -->--}}
{{--          </div>--}}
{{--          <!-- /.col -->--}}
{{--        </div>--}}
{{--        <!-- /.row -->--}}
{{--      </div>--}}
{{--@stop--}}

{{--@section('script')--}}
{{--<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--<script src="{{ URL::asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--<script src="{{ URL::asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
{{--<script src="{{ URL::asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>--}}
{{--<script>--}}
{{--  $(function () {--}}
{{--    $("#role_lists").DataTable({--}}
{{--      "responsive": true,--}}
{{--      "autoWidth": false,--}}
{{--    });--}}
{{--  });--}}


{{--</script>--}}

{{--@extends('layout/common-js')--}}
{{--@stop--}}
