@extends('layouts.app')

@section('pagetitle')
    <h1 class="m-0">{{ $page_title }}</h1>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Starter Page</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
{{--                    <h3 class="card-title">--}}
{{--                        <div class="pad_space">--}}
{{--                            <a href="{{ route('users.create') }}" class="btn btn-block btn-secondary btn-md" title="Add">--}}
{{--                                <i class="far fa-plus-square fa-lg"></i>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </h3>--}}
{{--                    <h3 class="card-title">--}}
{{--                        <div class="pad_space">--}}
{{--                            <a href="#" class="btn btn-block btn-success btn-md" title="Download List">--}}
{{--                                <i class="fas fa-file-download fa-lg"></i>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </h3>--}}
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {{ $dataTable->table() }}


                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_script')
    {{ $dataTable->scripts() }}
    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
@endsection
