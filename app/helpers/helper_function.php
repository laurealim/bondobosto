<?php

use App\Permission;
use App\Role;
use App\User;
use App\Models\Applicant;
use App\Models\Land;
use App\Models\District;
use App\Models\Upazila;
use App\Models\Union;


function check_view_permission($role)
{

}

function check_view_role($role){
    $roleArr = array($role);
    if (auth()->check() && auth()->user()->hasRole($roleArr)) {
        return true;
    } else {
        return false;
    }
}

function pr($array = array())
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

function getDistNameById($id){
    $distName = District::where('id','=',$id)
        ->select('bn_name')
        ->get()->toArray();
    return $distName[0]['bn_name'];
}

function getUpazilaNameById($id){
    $distName = Upazila::where('id','=',$id)
        ->select('bn_name')
        ->get()->toArray();
    return $distName[0]['bn_name'];
}

function getUnionNameById($id){
    $distName = Union::where('id','=',$id)
        ->select('bn_name')
        ->get()->toArray();
    return $distName[0]['bn_name'];
}

function bn2en($number)
{
    $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    return str_replace($bn, $en, $number);
}

function uniqueNid($nid, $id = null)
{
    try {
        if ($id) {
            Applicant::where('nid', '=', $nid)
                ->where('id', '!=', $id)
                ->firstOrFail();
        } else {
            Applicant::where('nid', '=', $nid)->firstOrFail();
        }
        return false;

    } catch (Exception $exception) {
        return true;
    }
}

function uniquePhone($phone)
{
    try {
        Applicant::where('phone', '=', $phone)->firstOrFail();
        return false;

    } catch (Exception $exception) {
        return true;
    }
}

function getLandById($id)
{
    try {
        if ($id) {
            $res = Land::where('id', '=', $id)
                ->where('is_active', '!=', config('constants.status.Deleted'))
                ->get()->toArray();
            $res[0]['dist'] = getDistNameById($id);
            $res[0]['upa'] = getUpazilaNameById($id);
            return $res;
        }
    } catch (Exception $exception) {
//        dd($exception);
    }
}

function getClientInfoByIds($ids){
    $idArr = explode(',',$ids);
    $clientListArray = Applicant::whereIn('id',$idArr)->get();
    return $clientListArray;

}


