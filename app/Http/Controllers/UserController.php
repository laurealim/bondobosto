<?php

namespace App\Http\Controllers;

use App\DataTables\EmployeeDataTable;
use App\Exports\EmployeeExport;
use App\Models\Applicant;
use App\Models\District;
use App\Models\Union;
use App\Models\Upazila;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Hash;
//use Maatwebsite\Excel\Facades\Excel; // when uses Excel::download,
use Maatwebsite\Excel\Excel; // when use Excel $excel,
use App\Models\Employee;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = 'সকল ইউজার';
//        pr(auth::user()->user_type);
//        dd();
//        config('constants.status.Deleted')
        if(auth::user()->user_type != 1){
            $conditionArr = [
                ['user_type','!=', 1],
            ];
        }else{
            $conditionArr = [];
        }

        $data = User::where($conditionArr)
            ->select('users.*', 'roles.name as role_name', 'roles.slug', 'districts.name as dist_name', 'upazilas.name as upaz_name')
            ->leftJoin('districts', 'districts.id', 'users.dist')
            ->leftJoin('upazilas', 'upazilas.id', 'users.upa')
            ->leftJoin('roles', 'roles.id', 'users.user_type')
            ->orderBy('users.id', 'asc')->get();

        $userRole = Role::orderby('id','asc')->pluck('name','id');
        $gender = config('constants.gender.arr');
//        dd($data);
//        $slug = Str::slug("This is it");
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '<a class="btn btn-success" id="edit-user" href= ' . route('users.edit', $row->id) . '>Edit </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id=' . $row->id . ' href=' . route('users.destroy', $row->id) . ' class="btn btn-danger delete-user">Delete</a>
<a id=' . $row->id . ' href= ' . route('users.resetpass') . ' class="btn btn-info pass-reset"><i class="fa fa-key" aria-hidden="true"></i></a>';
                    return $action;
                })
                ->addColumn('status', function ($row){
                    config('constants.status.'.$row->status);
                    $color = '#61A151';
                    if($row->status == 0)
                        $color = '#9d1e15';
                    $status = '<b><span style="color: '.$color.'">'.config('constants.status.'.$row->status).'</span></b>';

                    return $status;
                })
                ->editColumn('gender', function ($row) use($gender) {
                    return $gender[$row->gender];
                })
//                ->editColumn('dist', function ($row) use($userRole) {
//                    return $userRole[$row->user_type];
//                })
//                ->editColumn('upa', function ($row) use($userRole) {
//                    return $userRole[$row->user_type];
//                })

                ->rawColumns(['action','status'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('users.index', compact('page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'নতুন ইউজার তৈরী';
//        $countryData = Country::all();
        $districtListArr = District::pluck('bn_name', 'id');
        $upozillaListArr = Upazila::pluck('bn_name', 'id');
        $conditionArr = [];
        if(auth::user()->user_type === 1){
            $conditionArr = [
                ['id','!=', 1],
            ];
        }
        if(auth::user()->user_type === 2){
            $conditionArr = [
                ['id','!=', 1],
                ['id','!=', 2],
            ];
        }
        $roleList = Role::where($conditionArr)->orderby('id','asc')->get()->toArray();
        return view('users.create', compact('districtListArr', 'upozillaListArr', 'page_title','roleList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->all();
//        dd($formData);

        DB::beginTransaction();
        try {
            $phone = bn2en($formData['phone']);
            $nid = "";
            if(!empty($formData['nid'])){
                $nid = bn2en($formData['nid']);
            }

            $data = $this->validate($request, [
                'email' => 'required',
                'phone' => 'required',
                'name' => 'required',
                'dob' => 'required',
                'gender' => 'required',
                'user_type' => 'required',
                'dist' => 'required',
                'upa' => 'required',
            ], [
                'email.required' => 'Need',
                'phone.required' => 'Need',
                'name.required' => 'Need',
                'dob.required' => 'Need',
                'gender.required' => 'Need',
                'user_type.required' => 'Need',
                'dist.required' => 'Need',
                'upa.required' => 'Need',
            ]);

            $dob = date('Y-m-d', strtotime($formData['dob']));
            $resData = [
                'email' => $formData['email'],
                'phone' => $phone,
                'name' => $formData['name'],
                'dob' => $dob,
                'gender' => $formData['gender'],
                'user_type' => $formData['user_type'],
                'dist' => $formData['dist'],
                'upa' => $formData['upa'],
                'nid' => $nid,
                'created_by' => auth::user()->id,
                'password' => Hash::make('abc123'),
            ];
            $resArr = User::create($resData);
            $lastInsertedId = $resArr->id;

            DB::table('users_roles')->insert([
                'user_id' => $lastInsertedId,
                'role_id' => $formData['user_type']
            ]);

            DB::commit();
            return redirect('/users')->with('success', 'নতুন ইউজার যোর করা হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/users/create')->with('error', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'ইউজার তথ্য সংশোধন';
        try {
            $userData = User::where('id', $id)->firstOrFail();

            $districtListArr = District::pluck('bn_name', 'id');
            $upozillaListArr = Upazila::where('district_id', $userData->dist)->pluck('bn_name', 'id')->all();

            $conditionArr = [];
//            if(auth::user()->user_type === 1){
//                $conditionArr = [
//                    ['id','!=', 1],
//                ];
//            }
            if(auth::user()->user_type === 2){
                $conditionArr = [
                    ['id','!=', 1],
                    ['id','!=', 2],
                ];
            }
            $roleList = Role::where($conditionArr)->orderby('id','asc')->get()->toArray();
            return view('users.edit', compact('districtListArr', 'upozillaListArr', 'page_title', 'roleList', 'userData', 'id'));
        } catch (\Exception $exception) {
            dd($exception);
//            return redirect('/users/create')->with('error', $exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData = $request->all();

        DB::beginTransaction();
        try {
            $userData = User::find($id);

            $phone = bn2en($formData['phone']);
            $nid = $userData->nid;
            if(!empty($formData['nid'])) {
                $nid = bn2en($formData['nid']);
            }

            $data = $this->validate($request, [
                'email' => 'required',
                'phone' => 'required',
                'name' => 'required',
                'dob' => 'required',
                'gender' => 'required',
                'user_type' => 'required',
                'dist' => 'required',
                'upa' => 'required',
            ], [
                'email.required' => 'Need',
                'phone.required' => 'Need',
                'name.required' => 'Need',
                'dob.required' => 'Need',
                'gender.required' => 'Need',
                'user_type.required' => 'Need',
                'dist.required' => 'Need',
                'upa.required' => 'Need',
            ]);

            $dob = date('Y-m-d', strtotime($formData['dob']));

            $userData->email = $formData['email'];
            $userData->phone = $phone;
            $userData->name = $formData['name'];
            $userData->dob = $dob;
            $userData->nid = $nid;
            $userData->gender = $formData['gender'];
            $userData->user_type = $formData['user_type'];
            $userData->dist = $formData['dist'];
            $userData->upa = $formData['upa'];

            $lastInsertedId = $userData->save();

            $affected = DB::table('users_roles')
                ->where('user_id', $id)
                ->update(['role_id' => $formData['user_type']]);

            DB::commit();
            return redirect('/users')->with('success', 'তথ্য সফলভাবে সংশোধন হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect("/users/$id/edit")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ' . $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $userModel = new User();

        DB::beginTransaction();
        try {
            if (!empty($id)) {
                $updateData = [
                    'status' => config('constants.status.Deleted'),
                ];

                $userModel->where('id', '=', $id)->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function datatable(EmployeeDataTable $dataTable){
//        echo "asas";
//        dd("asdd");
        $page_title = 'সকল ইউজার';
        return $dataTable->render('users.employee',compact('page_title'));
    }

    public function export(Excel $excel)
    {
//        return Excel::download(new EmployeeExport, 'Employee_List.xlsx');
//        return (new EmployeeExport)->download('Employee_List.xlsx');
        return $excel->download(new EmployeeExport,'User_List.xlsx');

    }

    public function changepass(Request $request){

        $page_title = 'পাসওয়ার্ড পরিবর্তন';
        if($request->post()){
            $pass = $request->post('password');
            $c_pass = $request->post('c_pass');
            DB::beginTransaction();
            try{
                if($pass !== $c_pass){
                    return redirect('/change-password')->with('error', 'দুটি পাসওয়ার্ড মিলছে না।');
                }else{
                    //secrettt
                    $u_id = auth::user()->id;
                    $password = Hash::make($pass);
                    $affected = DB::table('users')
                        ->where('id', $u_id)
                        ->update(['password' => $password]);
                    DB::commit();
                    return redirect('/change-password')->with('success', 'পাসওয়ার্ড সফলভাবে পরিবর্তন হয়েছে।');
                }
            }
            catch (\Exception $exception){
                return redirect('/change-password')->with('error', 'পুনরায় চেষ্টা করুন।');
            }
        }

        return view('users.changepass',compact('page_title'));
    }

    public function resetpass(Request $request){

//        dd($request->post());
        $user_id = $request->post('id');
        DB::beginTransaction();
        try{
            if(empty($user_id)){
                $request->session()->flash('error', 'পাসওয়ার্ড পরিবর্তন সম্ভব হচ্ছে না...');
                return response()->json(['status' => 'error']);
            }else{
                //secrettt
                $password = Hash::make('abc123');
                $affected = DB::table('users')
                    ->where('id', $user_id)
                    ->update(['password' => $password]);
                DB::commit();

                $request->session()->flash('success', 'পাসওয়ার্ড পরিবর্তন সম্পন্ন হয়েছে...');
                return response()->json(['status' => 'success']);
            }
        }
        catch (\Exception $exception){

            $request->session()->flash('error', 'পাসওয়ার্ড পরিবর্তন সম্ভব হচ্ছে না...');
            return response()->json(['status' => 'error']);
        }
//        $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
//        return response()->json(['status' => 'success']);
    }
}
