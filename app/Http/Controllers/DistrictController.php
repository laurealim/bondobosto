<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Division;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function list()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $districts = new District();

        $districts = $districts->select('divisions.bn_name as division_name', 'districts.*')
            ->leftJoin('divisions', 'divisions.id', '=', 'districts.division_id')
            ->where('districts.name', 'like', '%' . $searchData . '%')
            ->orwhere('divisions.name', 'like', '%' . $searchData . '%')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('district.ajax_list', compact('districts'));
        } else {
            return view('district.adminList', compact('districts'));
        }
    }

    public function form()
    {
        $divisionModel = new Division();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        return view('district.adminForm', compact('divisionData'));
    }

    public function store(Request $request)
    {
        $districtModel = new District();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
            'division_id' => 'required',
        ], [
            'name.required' => 'District Name is required',
            'bn_name.required' => 'District Name (Bangla) is required',
            'division_id.required' => 'Please Select a Division',

        ]);

        $districtModel->saveData($request);
        return redirect('admin/district/list')->with('success', 'New District added successfully');
    }


    public function show(District $district)
    {
        //
    }


    public function edit($id)
    {
        $divisionModel = new Division();

        try {
            $divisionList = $divisionModel->pluck('bn_name', 'id')->all();
            $districtData = District::where('id', $id)->first();
            return view('district.adminEdit', compact('divisionList', 'districtData', 'id'));
        } catch (\Exception $exception) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
        }
    }


    public function update(Request $request, District $district)
    {
        $districtModel = new District();
        $data = $this->validate($request, [
            'name' => 'required',
            'bn_name' => 'required',
            'division_id' => 'required',
        ], [
            'name.required' => 'District Name is required',
            'bn_name.required' => 'District Name (Bangla) is required',
            'division_id.required' => 'Please Select a Division',

        ]);

        $districtModel->updateData($request);

        return redirect('admin/district/list')->with('success', 'District edited successfully');
    }


    public function destroy($id, Request $request)
    {
//        $apartmentModel = Apartment::findOrFail();
        $districtInfo = District::findOrFail($id);
        if (isset($request->id)) {
            $districtInfo->delete();
            $request->session()->flash('success', 'District Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'District Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function districtSelectAjaxList(Request $request)
    {
        return $this->_districtSelectAjaxList($request);
    }

    private function _districtSelectAjaxList($request)
    {
        if ($request->ajax()) {

            $districtModel = new District();
            $divisionID = $request->post('divisionID');
            $districts = $districtModel->where("division_id", $divisionID)->pluck("bn_name", "id")->all();
            return json_encode($districts);
        }
    }
}
