<?php

namespace App\Http\Controllers;

use App\Models\AssignLand;
use App\Models\Applicant;
use App\Models\District;
use App\Models\Land;
use App\Models\Union;
use App\Models\Upazila;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;

class AssignLandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = 'বন্দবস্ত বন্টন তালিকা';
        $data = AssignLand::latest('assign_lands.created_at')->where('assign_lands.is_active','!=',config('constants.status.Deleted'))
            ->select("assign_lands.id", "assign_lands.clients", "assign_lands.from", "assign_lands.to", "assign_lands.status", "lands.dist", "lands.upa", "lands.mouja", "lands.khotiyan", "lands.dag_no", "lands.lnd_size", "lands.lnd_price", "lands.case_no" )
            ->leftJoin('lands', 'assign_lands.land_id', 'lands.id')
            ->get();

        if ($request->ajax()) {
            $districtListArr = District::pluck('bn_name', 'id');
            $upazillaListArr = Upazila::pluck('bn_name', 'id');
            $unionListArr = Union::pluck('bn_name', 'id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '<a class="btn btn-success" id="edit-user" href= ' . route('assign_lands.edit', $row->id) . '>Edit </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id=' . $row->id . ' href=' . route('assign_lands.destroy', $row->id) . ' class="btn btn-danger delete-user">Delete</a>';
                    return $action;
                })
                ->addColumn('names', function ($row) {
                    $clientsArr = getClientInfoByIds($row->clients);

                    $str = "";
                    foreach ($clientsArr as $keys => $values){
                        $str .= $values->name.'('.$values->nid.'), <br/>';
                    }
                    return $str;
                })
                ->rawColumns(['action','names'])
                ->editColumn('union', function ($row) use($unionListArr) {
                    $union = "";
                    if($row->union >= 1){
                        $union = $unionListArr[$row->union];
                    }else{
                        $union = 'পৌরসভা';
                    }
                    return $union;
                })
                ->editColumn('dist', function ($row) use($districtListArr) {
                    return $districtListArr[$row->dist];
                })
                ->editColumn('upa', function ($row) use($upazillaListArr) {
                    return $upazillaListArr[$row->upa];
                })
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('assign_lands.index', compact('page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'বন্দবস্ত বন্টন ফর্ম';
        $landList = Land::pluck('case_no', 'id');
        $cilentList = Applicant::select('id', DB::raw("CONCAT(name,' (',nid,') ') AS full_name"))
            ->where('is_active',"!=",config('constants.status.Deleted'))
            ->where('status',"=",config('constants.landStatus.Unassigned'))
            ->get()
            ->pluck('full_name', 'id');
        $districtListArr = District::pluck('bn_name', 'id');
        $upozillaListArr = Upazila::pluck('bn_name', 'id');
        return view('assign_lands.create', compact('districtListArr', 'upozillaListArr', 'page_title', 'cilentList','landList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assignLandModel = new AssignLand();

        DB::beginTransaction();
        try{
            $formData = $request->all();
            $clientList = '';
            $clientArr = $formData['clients'];
            foreach ($clientArr as $key => $vals){
                $clientList = $vals .','.$clientList;
            }
            $clientList = rtrim($clientList, ", ");
            $request->request->add(['clients' => $clientList]);
            $resData = $assignLandModel->saveData($request);

            DB::commit();
            return redirect('/assign_lands')->with('success', 'Assigned successfully');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect('/assign_lands/create')->with('error', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssignLand  $assignLand
     * @return \Illuminate\Http\Response
     */
    public function show(AssignLand $assignLand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssignLand  $assignLand
     * @return \Illuminate\Http\Response
     */
    public function edit(AssignLand $assignLand)
    {
        $page_title = 'বন্দবস্ত বন্টন তথ্য সংশোধন ফর্ম';
        try {
//            $assignLandData = $assignLandModel->where('assign_lands.id', $id)->firstOrFail();
            $assignLand->clients = explode(',',$assignLand->clients);
            $id = $assignLand->id;

            $landArr = Land::pluck('case_no', 'id');
            $landDataArr = Land::where('id','=',$assignLand->land_id)->get()->toArray();
            $landDataArr = $landDataArr[0];
            $cilentList = Applicant::select('id', DB::raw("CONCAT(name,' (',nid,') ') AS full_name"))
                ->where('is_active',"!=",config('constants.status.Deleted'))
                ->where('status',"=",config('constants.landStatus.Unassigned'))
                ->get()
                ->pluck('full_name', 'id');
            return view('assign_lands.edit', compact( 'page_title','assignLand', 'id','landArr','landDataArr','cilentList'));
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssignLand  $assignLand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assignLandModel = new AssignLand();

        DB::beginTransaction();
        try{
            $formData = $request->all();
            $clientList = '';
            $clientArr = $formData['clients'];
            foreach ($clientArr as $key => $vals){
                $clientList = $vals .','.$clientList;
            }
            $clientList = rtrim($clientList, ", ");
            $request->request->add(['clients' => $clientList]);
            $request->request->add(['id' => $id]);
            $resData = $assignLandModel->updateData($request);

            DB::commit();
            return redirect('/assign_lands')->with('success', 'Assigned successfully');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect("/assign_lands/{$id}/edit")->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssignLand  $assignLand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, AssignLand $assignLand)
    {
        $assignLandModel = new AssignLand();

        DB::beginTransaction();
        try {
            if (!empty($assignLand->id)) {
                $updateData = [
                    'is_active' => config('constants.status.Deleted'),
                ];

                $assignLandModel->where('id', '=', $assignLand->id)->update($updateData);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }
}
