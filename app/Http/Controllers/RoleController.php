<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Role;
use Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = 'সকল রোল ';
        $data = Role::orderBy('id', 'asc')->get();
//        $slug = Str::slug("This is it");
//        dd($slug);
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $action = '<a class="btn btn-success" id="edit-role" href= ' . route('roles.edit', $row->id) . '>Edit </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id=' . $row->id . ' href=' . route('roles.destroy', $row->id) . ' class="btn btn-danger delete-role">Delete</a>';
                    return $action;
                })
                ->rawColumns(['action'])
                ->make(true);
//            <a class="btn btn-info" id="show-user" href=' . route('lands.show', $row->id) . '>Show</a>
        }

        return view('roles.index', compact('page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'ইউজার রোল তৈরী';
        return view('roles.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->all();

        DB::beginTransaction();
        try {
            $role_name = $formData['name'];
            $slug = Str::slug($role_name);
            $request->request->add(['slug' => $slug]);

            $data = $this->validate($request, [
                'name' => 'required',
            ], [
                'name.required' => 'অনুগ্রহ করে ইউজার রোল প্রদাল করুন',
            ]);

            $dev_role = new Role();
            $dev_role->slug = $slug;
            $dev_role->name = $role_name;
            $dev_role->save();

//            $lastInsertedId = $landModel->saveData($request);

            DB::commit();
            return redirect('/roles')->with('success', 'নতুন ইউজার রোল যুক্ত হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect('/roles/create')->with('error', $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id `
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'ইউজার রোল সংশোধন';
//        $countryData = Country::all();
        $roleModel = new Role();
        try {
            $roleData = $roleModel->where('roles.id', $id)
                ->firstOrFail();

            return view('roles.edit', compact('page_title', 'roleData', 'id'));
        } catch (\Exception $exception) {
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData = $request->all();
        DB::beginTransaction();
        try {
            $request->request->add(['id' => $id]);
            $data = $this->validate($request, [
                'name' => 'required',
            ], [
                'name.required' => 'অনুগ্রহ করে ইউজার রোল প্রদাল করুন',
            ]);

            $role_name = $formData['name'];

            $data = Role::find($id);
            $data->name = $role_name;

            $data->save();
//            dd($request);
            DB::commit();

//            return redirect()->back()->with('success', 'data updated');
            return redirect('/roles')->with('success', 'তথ্য সফলভাবে সংশোধন হয়েছে।');
        } catch (\Exception $exception) {
            DB::rollback();
            dd($exception);
            return redirect("/roles/$id/edit")->with('error', 'তথ্য সংশোধন সম্ভব হয় নি। ' . $exception->getMessage());
//			$request->session()->flash('errors', $exception->getMessage());
            //$request->session()->flash('errors', $exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            if (!empty($id)) {
                Role::destroy($id);
                DB::commit();
                $request->session()->flash('success', 'তথ্য সফলভাবে নুছে ফেলা হয়েছে...');
                return response()->json(['status' => 'success']);
            } else {
                DB::rollback();
                $request->session()->flash('errors', 'কোন তথ্য পাওয়া জায় নি...');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            $request->session()->flash('errors', 'তথ্য সফলভাবে মুছা সম্ভব হয় নি...');
            return response()->json(['status' => 'error']);
        }
    }

    public function roleSetting()
    {
        $page_title = 'রোল সেটিং';
        $roleListArr = Role::orderby('id', 'asc')->get();
        $permissionListArr = Permission::orderby('id', 'asc')->get();

        $results = DB::select('select roles_permissions.*, permissions.controller  from roles_permissions left join permissions on(roles_permissions.permission_id = permissions.id) WHERE controller is not NULL');
//            ->leftJoin('permissions', 'permissions.id', 'roles_permissions.permission_id')->get();
        $rolPermArray = [];
//        dd($results);
        foreach ($results as $key =>$val){
            $controller = $val->controller;
            $rId = $val->role_id;
            $permId = $val->permission_id;

//            if(!isset($rolPermArray[$controller]))
//            $rolPermArray[$controller] = [];

            if(!isset($rolPermArray[$rId]))
                $rolPermArray[$rId] = [];

            if(!isset($rolPermArray[$rId][$permId]))
                $rolPermArray[$rId][$permId] = $permId;
        }

//        dd($rolPermArray);
//        pr($rolPermArray);

//        dd($permissionListArr);
        if (isset($_POST['role_arr'])) {
//            dd($_POST);
        }
        return view('roles.role_setting', compact('page_title', 'roleListArr', 'permissionListArr','rolPermArray'));
    }

    public function updateRoleSetting(Request $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->post('id');
            $status = $request->post('status');
            $explodData = explode('_', $id);
            if ($status === 'true') {
                echo "true";
                DB::insert('insert into roles_permissions (role_id, permission_id) values (?, ?)', [$explodData[0], $explodData[1]]);
                DB::commit();
                $resData = [
                    'status' => 'success',
                ];

                return json_encode($resData);
            } else {
                echo "false";
                $deleted = DB::delete('delete from roles_permissions where role_id = ? and permission_id = ?', [$explodData[0], $explodData[1]]);
                DB::commit();
                $resData = [
                    'status' => 'success',
                ];

                return json_encode($resData);
            }
//            pr($explodData);
//            dd($request->post());
        } catch (\Exception $exception) {
            DB::rollback();
            $resData = [
                'status' => 'error',
                'msg' => $exception
            ];

            return json_encode($resData);
        }


    }
}
