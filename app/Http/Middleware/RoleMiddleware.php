<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission = null)
    {

        if(empty($request->user())){
            return redirect("/")->with('error', 'অনুগ্রহ করে পুনরায়  লগইন করুণ');
        }

        $roleArr = explode('|',$role);
        if (!$request->user()->hasRole($roleArr)) {
            abort(404);
        }

        if ($permission !== null && !$request->user()->can($permission)) {

            abort(404);
        }

        return $next($request);
    }
}
