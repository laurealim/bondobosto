<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = [
        'name','bn_name', 'url',
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }

        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $ticket->$key = $value;
            }
        }

        $ticket->save();
        return 1;
    }


    public function districts()
    {
        return $this->hasMany('App\Models\District')->orderBy('name', 'ASC');
    }
}
