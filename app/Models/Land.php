<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    protected $fillable = [
        'dist', 'upa', 'mouja', 'khotiyan', 'dag_no', 'lnd_size', 'lnd_cat', 'lnd_price', 'case_no', 'created_by', 'updated_by', 'status', 'is_active'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
        $this->status = config('constants.landStatus.Unassigned');;
        $this->is_active = config('constants.status.Active');;
//        $this->dob = date('Y-m-d', strtotime($data->dob));
//        dd($this);
        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
//        dd($data['id']);
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
//        $ticket->dob = date('Y-m-d', strtotime($ticket->dob));
        $ticket->save();
        return 1;
    }

//    public function applicants()
//    {
//        return $this->hasOne('App\Models\Applicant', 'training_id', "id");
//    }
}
