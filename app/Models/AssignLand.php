<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignLand extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'status', 'is_active'
    ];

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
        $this->status = config('constants.landStatus.Unassigned');
        $this->is_active = config('constants.status.Active');
        $this->from = date("Y");
        $this->to = date("Y") + 1;
//        $dob = $data->dob;
//        $this->dob = date('Y-m-d', strtotime($data->dob));

//        pr($dob);
//        dd($this);
        $this->save();
        return $this->id;
    }

    public function updateData($data)
    {
//        dd($data['id']);
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token" && $key != "_method") {
                $ticket->$key = $value;
            }
        }
        $ticket->updated_by = auth()->user()->id;
//        $ticket->dob = date('Y-m-d', strtotime($ticket->dob));
        $ticket->save();
        return 1;
    }

    public function lands()
    {
        return $this->hasMany('App\Models\Lands')->orderBy('land_id', 'ASC');
    }
}
