<?php

namespace App\Exports;

use App\Models\Employee;
use App\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class EmployeeExport implements ShouldAutoSize, WithMapping, WithHeadings, WithEvents, FromQuery, WithCustomStartCell
{
    use Exportable;

    /**
     * @return \Illuminate\Support\Collection
     */
//    public function collection()  // have to implement "FromCollection"
//    {
//
////        return Employee::all();
////        return User::all();
//        $userModel = new User();
//        $resData = $userModel->where('status', '=', 1)
//            ->select('users.name', 'users.email', 'roles.name as role_name', 'roles.slug')
//            ->leftJoin('roles', 'roles.id', 'users.user_type')
//            ->get();
//
//        return $resData;
//    }

    public function query()
    {

//        return Employee::all();
//        return User::all();
        $userModel = new User();
        $resData = $userModel->query()->select('users.*', 'roles.name as role_name', 'roles.slug', 'districts.name as dist_name', 'upazilas.name as upaz_name')
            ->leftJoin('roles', 'roles.id', 'users.user_type')
            ->leftJoin('districts', 'districts.id', 'users.dist')
            ->leftJoin('upazilas', 'upazilas.id', 'users.upa');

        return $resData;

//        return Employee::query()->where('id','>',50);
    }

    public function map($user): array
    {
//        dd($user);
        // which column have to display
        return [
            $user->id,
            $user->name,
            $user->email,
            $user->phone,
            $user->dist_name,
            $user->upaz_name,
            $user->nid ? $user->nid : "--",
            $user->gender == 1 ? "Male" : "Female",
            $user->status == 1 ? "Active" : "Deleted",
        ];
    }

    public function headings(): array
    {
        // For Table Heading
        return [
            "#",
            "Name",
            "Email",
            "Phone No.",
            "District Name",
            "Upazilla Name",
            "NID No.",
            "Gender",
            "Status",
        ];
    }

    public function registerEvents(): array
    {
        // For table design
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle("A1:I1")->applyFromArray(
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
            }
        ];
    }

    public function startCell(): string
    {
        // For Starting Row
        return "A1";
    }
}
